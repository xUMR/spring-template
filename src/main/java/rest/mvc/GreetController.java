package rest.mvc;

import core.models.Message;
import core.models.entities.Person;
import core.services.MessageService;
import core.services.PersonService;
import core.services.exceptions.PersonNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rest.resources.PersonViewModel;

/**
 * Created by user on 5.5.2016.
 */
@Controller
@RequestMapping("/greet")
public class GreetController {

    private PersonService personService;
    private MessageService messageService;

    @Autowired
    public GreetController(PersonService personService,
                           MessageService messageService) {
        this.personService = personService;
        this.messageService = messageService;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/name/{name}")
    public ResponseEntity<Message> greetByName(@PathVariable String name) {
        Message message = messageService.Greet(name);
        return ResponseEntity.ok(message);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/id/{id}")
    public ResponseEntity<Message> greetById(@PathVariable Long id) {
        Person person = null;
        try {
            person = personService.getPerson(id);
            Message message = messageService.Greet(person);
            return ResponseEntity.ok(message);
        } catch (PersonNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/person/{name}")
    public ResponseEntity<Message> greetByPerson(@PathVariable String name) {
        Person person = null;
        try {
            person = personService.getPerson(name);
            Message message = messageService.Greet(person);
            return ResponseEntity.ok(message);
        } catch (PersonNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }
}
