package rest.mvc;

import core.models.Message;
import core.services.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by user on 5.5.2016.
 */
public class HelloWorldController {

    @Autowired
    MessageService messageService;

    @RequestMapping(method = RequestMethod.GET, value = "/view")
    public String view() {
        return "view";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/json")
    public ResponseEntity<Message> json() {
        Message message = messageService.Greet();
        return ResponseEntity.ok(message);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/404")
    public ResponseEntity<Message> notFound() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }
}
