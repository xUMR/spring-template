package rest.mvc;

import core.models.entities.Person;
import core.services.PersonService;
import core.services.exceptions.PersonAlreadyExistsException;
import core.services.exceptions.PersonNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rest.resources.PersonViewModel;

/**
 * Created by user on 5.5.2016.
 */
@Controller
@RequestMapping("/person")
public class PersonController {

    @Autowired
    private PersonService personService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<PersonViewModel> add(@RequestBody PersonViewModel request) {
        try {
            Person person = personService.addPerson(request.toEntity());
            PersonViewModel body = PersonViewModel.fromEntity(person);
            return ResponseEntity.ok(body);
        } catch (PersonAlreadyExistsException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<PersonViewModel> get(@PathVariable Long id) {
        try {
            Person person = personService.getPerson(id);
            return ResponseEntity.ok(PersonViewModel.fromEntity(person));
        } catch (PersonNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }
}
