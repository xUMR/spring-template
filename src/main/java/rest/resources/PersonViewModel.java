package rest.resources;

import core.models.entities.Person;

/**
 * Created by user on 5.5.2016.
 */
public class PersonViewModel {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Person toEntity() {
        Person p = new Person();
        p.setName(name);
        System.out.println("(viewmodel) name is " + name);
        return p;
    }

    public static PersonViewModel fromEntity(Person person) {
        PersonViewModel p = new PersonViewModel();
        p.setName(person.getName());

        return p;
    }
}
