package rest.exceptions;

/**
 * Created by user on 5.5.2016.
 */
public class NotFoundException extends RuntimeException {
    public NotFoundException(Throwable cause) {
        super(cause);
    }

    public NotFoundException() {
        super();
    }
}
