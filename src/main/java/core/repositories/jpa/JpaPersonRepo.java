package core.repositories.jpa;

import core.models.entities.Person;
import core.repositories.PersonRepo;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by user on 5.5.2016.
 */
@Repository
public class JpaPersonRepo implements PersonRepo {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Person> allPersons() {
        Query query = em.createQuery("SELECT a FROM Person a");

        return query.getResultList();
    }

    @Override
    public Person getPerson(Long id) {
        return em.find(Person.class, id);
    }

    @Override
    public Person getPerson(String name) {
        Query query = em.createQuery("SELECT a FROM Person a WHERE a.name=?1");
        query.setParameter(1, name);
        List<Person> persons = query.getResultList();

        if (persons.size() == 0)
            return null;

        return persons.get(0);
    }

    @Override
    public Person addPerson(Person person) {
        em.persist(person);

        return person;
    }
}
