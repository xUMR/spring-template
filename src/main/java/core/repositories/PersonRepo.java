package core.repositories;

import core.models.entities.Person;

import java.util.List;

/**
 * Created by user on 5.5.2016.
 */
public interface PersonRepo {
    public List<Person> allPersons();
    public Person getPerson(Long id);
    public Person getPerson(String name);
    public Person addPerson(Person person);
}
