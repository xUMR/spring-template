package core.services.impl;

import core.models.entities.Person;
import core.repositories.PersonRepo;
import core.services.PersonService;
import core.services.exceptions.PersonAlreadyExistsException;
import core.services.exceptions.PersonNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by user on 5.5.2016.
 */
@Service
@Component
@Transactional
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonRepo personRepo;

    @Override
    public List<Person> allPersons() {
        return personRepo.allPersons();
    }

    @Override
    public Person getPerson(Long id) throws PersonNotFoundException {
        Person person = personRepo.getPerson(id);
        if (person == null)
            throw new PersonNotFoundException();

        return person;
    }

    public Person getPerson(String name) throws PersonNotFoundException {
        Person person = personRepo.getPerson(name);
        if (person == null)
            throw new PersonNotFoundException();

        return person;
    }

    @Override
    public Person addPerson(Person person) throws PersonAlreadyExistsException {
        boolean personExists = personRepo.getPerson(person.getName()) != null;
        if (personExists)
            throw new PersonAlreadyExistsException();

        return personRepo.addPerson(person);
    }
}
