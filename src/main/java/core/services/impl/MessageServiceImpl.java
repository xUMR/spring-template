package core.services.impl;

import core.models.Message;
import core.models.entities.Person;
import core.repositories.PersonRepo;
import core.services.MessageService;
import core.services.exceptions.PersonNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by user on 5.5.2016.
 */
@Service
@Component
@Transactional
public class MessageServiceImpl implements MessageService {

    @Autowired
    private PersonRepo personRepo;

    @Override
    public Message Greet() {
        Message message = new Message();
        message.setContent("Hello World!");

        return message;
    }

    @Override
    public Message Greet(String name) {
        Message message = new Message();
        name = name.toUpperCase().substring(0, 1) + name.substring(1);
        message.setContent("Hello, " + name + "!");

        return message;
    }

    @Override
    public Message Greet(Person person) {
        Message message = new Message();
        message.setContent("Hello, " + person.getName() + "!");

        return message;
    }
}
