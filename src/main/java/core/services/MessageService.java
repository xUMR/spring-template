package core.services;

import core.models.Message;
import core.models.entities.Person;

/**
 * Created by user on 5.5.2016.
 */
public interface MessageService {
    public Message Greet();
    public Message Greet(String name);
    public Message Greet(Person person);
}
