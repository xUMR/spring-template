package core.services.exceptions;

/**
 * Created by user on 5.5.2016.
 */
public class PersonAlreadyExistsException extends Exception {
    public PersonAlreadyExistsException() {
        super();
    }

    public PersonAlreadyExistsException(String message) {
        super(message);
    }

    public PersonAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }
}
