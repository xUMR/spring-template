package core.services.exceptions;

/**
 * Created by user on 5.5.2016.
 */
public class PersonNotFoundException extends Exception {
    public PersonNotFoundException() {
        super();
    }

    public PersonNotFoundException(String message) {
        super(message);
    }

    public PersonNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
