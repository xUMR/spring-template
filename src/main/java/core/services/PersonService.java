package core.services;

import core.models.entities.Person;
import core.services.exceptions.PersonAlreadyExistsException;
import core.services.exceptions.PersonNotFoundException;

import java.util.List;

/**
 * Created by user on 5.5.2016.
 */
public interface PersonService {
    public List<Person> allPersons();
    public Person getPerson(Long id) throws PersonNotFoundException;
    public Person getPerson(String name) throws PersonNotFoundException;
    public Person addPerson(Person person) throws PersonAlreadyExistsException;
}
