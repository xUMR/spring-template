package core.models;

/**
 * Created by user on 5.5.2016.
 */
public class Message {
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
